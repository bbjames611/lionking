import org.junit.Test;

import static org.junit.Assert.*;

public class MainTest {


    @Test
    public void returnFirst() {
        int first = Main.returnFirst();

        assertEquals(1, first);
    }
}